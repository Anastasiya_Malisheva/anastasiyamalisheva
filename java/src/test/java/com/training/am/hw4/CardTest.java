package com.training.am.hw4;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    @Test
    public void getName_getValidName_validName() {
        String expected = "Ivan";
        Card card = new Card(expected);

        String actual = card.getName();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void setName_setValidName_validName() {
        String initial = "Ivan";
        String expected = "Vanya";
        Card card = new Card(initial);

        card.setName(expected);
        String actual = card.getName();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getBalance_getValidBalance_validBalance() {
        double expected = 10500;
        Card card = new Card("Vanya", expected);

        double actual = card.getBalance();

        Assert.assertEquals(expected, actual, 0);
    }

    @Test
    public void setBalance_setValidBalance_validBalance() {
        double initial = 10500;
        double expected = 20500;
        Card card = new Card("Vanya", initial);

        card.setBalance(expected);
        double actual = card.getBalance();

        Assert.assertEquals(expected, actual, 0);
    }

    @Test
    public void increaseBalance_increaseBalanceCorrectly_validIncreasedBalance() {
        double initial = 10500;
        double increase = 10000;
        double expected = 20500;
        Card card = new Card("Vanya", initial);

        card.increaseBalance(increase);
        double actual = card.getBalance();

        Assert.assertEquals(expected, actual, 0);
    }

    @Test
    public void decreaseBalance_decreaseBalanceCorrectly_validDecreasedBalanced() {
        double initial = 20500;
        double decrease = 10000;
        double expected = 10500;
        Card card = new Card("Vanya", initial);

        card.decreaseBalance(decrease);
        double actual = card.getBalance();

        Assert.assertEquals(expected, actual, 0);
    }

    @Test
    public void convertBalance_convertBalanceCorrectly_validConvertedBalance() {
        double initial = 1000;
        double rate = 2.5;
        double expected = 2500;
        Card card = new Card("Vanya", initial);

        card.convertBalance(rate);
        double actual = card.getBalance();

        Assert.assertEquals(expected, actual, 0);
    }
}