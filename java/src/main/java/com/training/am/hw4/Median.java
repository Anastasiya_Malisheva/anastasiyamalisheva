package com.training.am.hw4;

import java.util.Arrays;

public class Median {
    public static float median(int[] massive) {
        float median;
        Arrays.sort(massive);
        int averageIndex = massive.length / 2;
        if (massive.length % 2 == 0) {
            median = (float) ((massive[averageIndex] + massive[averageIndex - 1]) / 2.0);
        } else {
            averageIndex = Math.round(averageIndex);
            median = massive[averageIndex];
        }
        return median;
    }

    public static double median(double[] massive) {
        double median;
        Arrays.sort(massive);
        int averageIndex = massive.length / 2;
        if (massive.length % 2 == 0) {
            median = (float) ((massive[averageIndex] + massive[averageIndex - 1]) / 2.0);
        } else {
            averageIndex = Math.round(averageIndex);
            median = massive[averageIndex];
        }
        return median;
    }
}
