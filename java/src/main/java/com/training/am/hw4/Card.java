package com.training.am.hw4;

public class Card {
    private String name;
    private double balance;

    public Card(String name) {
        this.name = name;

    }

    public Card(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void increaseBalance(double balance) {
        this.balance += balance;
    }
    public void decreaseBalance(double balance) {
        this.balance -= balance;
    }
    public void convertBalance(double rate) {
        this.balance *= rate;
    }
}

