package com.training.am.hw2;

public class AlgorithmCalculator {

    public static final int LOOP_TYPE_WHILE = 1;
    public static final int LOOP_TYPE_DO_WHILE = 2;
    public static final int LOOP_TYPE_FOR = 3;
    public static final int ALGORITHM_TYPE_FIBONACCI = 1;
    public static final int ALGORITHM_TYPE_FACTORIAL = 2;

    public static void main(String[] args) {
        int algorithmId = Integer.parseInt(args[0]);
        int loopType = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);
        int result;
        if (algorithmId == ALGORITHM_TYPE_FIBONACCI) {
            int a = 1;
            int b = 1;
            int i = 3;
            System.out.println(a + " " + b);
            if (loopType == LOOP_TYPE_WHILE) {
                while (i <= n) {
                    result = a + b;
                    printResult(result);
                    i++;
                    a = b;
                    b = result;
                }
            }
            if (loopType == LOOP_TYPE_DO_WHILE) {
                do {
                    result = a + b;
                    printResult(result);
                    i++;
                    a = b;
                    b = result;
                } while (i <= n);
            }
            if (loopType == LOOP_TYPE_FOR) {
                for (; i <= n; i++) {
                    result = a + b;
                    printResult(result);
                    a = b;
                    b = result;
                }

            }
        }
        if (algorithmId == ALGORITHM_TYPE_FACTORIAL) {
            result = 1;
            int i = 1;
            if (loopType == LOOP_TYPE_WHILE) {
                while (i <= n) {
                    result = result * i;
                    i++;
                }
                printResult(result);
            }
            if (loopType == LOOP_TYPE_DO_WHILE) {
                do {
                    result = result * i;
                    i++;
                } while (i <= n);
                printResult(result);
            }
            if (loopType == LOOP_TYPE_FOR) {
                for (; i <= n; i++) {
                    result = result * i;
                }
                printResult(result);
            }
        }
    }

    private static void printResult(int message) {
        System.out.println(message);
    }


}
