package com.training.am.hw3;

public class Initialization {
    static int intVariable;
    static boolean booleanVariable;
    static char charVariable;
    static byte byteVariable;
    static short shortVariable;
    static long longVariable;
    static float floatVariable;
    static double doubleVariable;
    static String stringVariable;

    public static void main(String[] args) {
        System.out.println("int default value is " + intVariable);
        System.out.println("boolean default value is " + booleanVariable);
        System.out.println("char default value is " + charVariable);
        System.out.println("byte default value is " + byteVariable);
        System.out.println("short default value is " + shortVariable);
        System.out.println("long default value is " + longVariable);
        System.out.println("float default value is " + floatVariable);
        System.out.println("double default value is " + doubleVariable);
        System.out.println("string default value is " + stringVariable);
    }
}
