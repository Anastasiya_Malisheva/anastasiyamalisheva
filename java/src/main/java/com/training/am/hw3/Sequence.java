package com.training.am.hw3;

public class Sequence {
    public static class InnerSequence {
        static int staticVariable = initialize("Initialize static field");
        int simpleVariable = initialize("Initialize non-static field");

        public InnerSequence() {
            initialize("Create object");
        }
            static int initialize(String name) {
            System.out.println(name);
            return 8;
        }

        static {
            staticVariable = initialize("Initialize static block");
        }

        {
            simpleVariable = initialize("Initialize non-static block");
        }
    }
        public static void main(String[] args) {
        InnerSequence linkedVariable;
            linkedVariable = new InnerSequence();
        }
}
