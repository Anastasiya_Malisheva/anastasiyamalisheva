package com.training.am.hw3;

public class Recursion {
    public static void main(String[] args) {
        System.out.println(recurse(0, 0, 1));
    }

    public static int recurse(int startFrom, int int1, int int2) {
        startFrom++;
        if (int1 + int2 < 0) {
            return startFrom;
        } else {
            return recurse(startFrom, Math.max(int1, int2), int1 + int2);
        }
    }
}

